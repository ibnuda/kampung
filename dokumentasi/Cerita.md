## Pelanggan

### OK
- Masuk sistem.
  - Method: POST.
  - Auth: Tidak.
  - Path: `/login`.
  - Payload: `login.json`.
### Belum
- Lihat tagihan tunggak.
  - Method: GET.
  - Auth: Ya.
  - Path: `/pelanggan/tagihan/tunggak`.
  - Payload:
### OK
- Lihat riwayat tagihan.
  - Method: GET.
  - Auth: Ya.
  - Path: `/pelanggan/tagihan`.
  - Payload:
### Belum
- Ganti password.
  - Method: PUT.
  - Auth: Ya.
  - Path: `/pelanggan`.
  - Payload: `ganti-password.json`.
### Belum
- Lihat tarif.
  - Method: GET.
  - Auth: Ya.
  - Path: `/tarif`
  - Payload:

## Petugas

### OK
- Masuk sistem.
  - Method: POST.
  - Auth: Tidak.
  - Path: `/login`.
  - Payload: `login.json`.
### Belum
- Lihat semua riwayat penggunaan air pelanggan.
  - Method: GET.
  - Auth: Ya.
  - Path: `/petugas/air`.
  - Param:
    - Bulan: default tahun ini.
    - Tahun: default bulan ini.
  - Payload:
### OK
- Menambah data penggunaan air pelanggan.
  - Method: POST.
  - Auth: Ya.
  - Path: `/petugas/air`.
  - Payload: `tambah-air.json`.
### OK
- Mengubah penggunaan air pelanggan di bulan itu.
  - Method: PUT.
  - Auth: Ya.
  - Path: `/petugas/air`.
  - Payload: `tambah-air.json`.
### Belum
- Ganti password.
  - Method: POST.
  - Auth: Ya.
  - Path: `/petugas`.
  - Payload: `ganti-password.json`.
### Belum
- Lihat tarif.
  - Method: GET.
  - Auth: Ya.
  - Path: `/tarif`
  - Payload:

## Administrator

### OK
- Masuk sistem.
  - Method: POST.
  - Auth: Tidak.
  - Path: `/login`.
  - Payload: `login.json`.
### Belum
- Melihat semua riwayat penggunaan air.
  - Method: GET.
  - Auth: Ya.
  - Path: `/admin/air`.
  - Param:
    - `tahun`: default tahun ini.
    - `bulan`: default bulan ini.
  - Payload:
### Belum
- Melihat semua riwayat tagihan.
  - Method: GET.
  - Auth: Ya.
  - Path: `/admin/tagihan`
  - Payload: 
### Belum
- Mengubah data tagihan untuk membayar.
  - Method: POST.
  - Auth: Ya.
  - Path: `/admin/tagihan`.
  - Payload: `bayar-tagihan.json`
### OK
- Menambah data pelanggan.
  - Method: POST.
  - Auth: Ya.
  - Path: `/admin/pelanngan`
  - Payload: `pelanggan-baru.json`.
### OK
- Melihat data pelanggan.
  - Method: GET.
  - Auth: Ya.
  - Path: `/admin/pelanggan/:notelp`
  - Payload:
### OK
- Melihat penggunaan air pelanggan.
  - Method: GET.
  - Auth: Ya.
  - Path: `/admin/air/:notelp`
  - Payload:
### Belum
- Melihat tagihan pelanggan.
  - Method: GET.
  - Auth: Ya.
  - Path: `/admin/tagihan/:notelp`
  - Payload:
### Belum
- Mengubah data pelanngan.
  - Method: PUT.
  - Auth: Ya.
  - Path: `/admin/pelanggan/:id`
  - Payload: `pelanggan-baru.json`.
### Belum
- Menambah data petugas.
  - Method: POST.
  - Auth: Ya.
  - Path: `/admin/petugas`.
  - Payload: `pelanggan-baru.json`.
### Belum
- Melihat data petugas.
  - Method: GET.
  - Auth: Ya.
  - Path: `/admin/petugas/:id`
  - Payload:
### Belum
- Mengubah data petugas.
  - Method: PUT.
  - Auth: Ya.
  - Path: `/admin/petugas/:id`
  - Payload: `pelanggan-baru.json`.
### Belum
- Lihat rate.
  - Method: GET.
  - Auth: Ya.
  - Path: `/rate`
  - Payload:

## Super Administrator

### OK
- Masuk sistem.
  - Method: POST.
  - Auth: Tidak.
  - Path: `/login`.
  - Payload: `login.json`.
### Belum
- Menambah data administrator.
  - Method: POST.
  - Auth: Ya.
  - Path: `/secret/admin`.
  - Payload: `pelanggan-baru.json`.
### Belum
- Melihat semua data administrator.
  - Method: GET.
  - Auth: Ya.
  - Path: `/secret/admin`
  - Payload:
### Belum
- Melihat data administrator.
  - Method: GET.
  - Auth: Ya.
  - Path: `/secret/admin/:id`
  - Payload:
### Belum
- Mengangkat administrator.
  - Method: POST.
  - Auth: Ya.
  - Path: `/secret/admin`
  - Payload: `angkat-admin.json`
### Belum
- Mengubah data administrator.
  - Method: PUT.
  - Auth: Ya.
  - Path: `/secret/admin/:id`
  - Payload: `pelanggan-baru.json`.
