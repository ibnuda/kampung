### `pelanggan-baru.json`
```
{ "nama": "nama pengguna."
, "nomor_telepon": "6288 dst dst."
, "password": "rahasia."
, "alamat": "kentingan."
}
```
### `login.json`
```
{ "nomor_telepon": "6288 dst dst."
, "password": "git gud."
}
```
### `pelanggantoken.json`
```
{ "nama": "nama pengguna."
, "nomor_telepon": "6288 dst dst."
, "group_id": 69
, "token": "jwt.token.disini."
}
```
### `ganti-password.json`
```
{ "password_lama": "6288 dst dst."
, "password_baru": "6288 dst dst."
}
```
### `tarif.json`
```
{ "tarif":
    [ { "mulai": 0
      , "sampai": 10
      , "harga": 0
      }
    , { "mulai": 11
      , "sampai": 20
      , "harga": 1000
      }
    , { "mulai": 21
      , "harga": 2000
      }
    ]
}
```
### `data-pengguna.json`
```
{ "nama": "nama sapa aja."
, "telepon": "telepon guwa dong."
, "alamat": "situ."
, "grup": "bisa SuperAdmin, Admin, Petugas, atau Pelanggan."
}
```
### `tagihan-pelanggan.json`
```
{ "nomor_telepon": "6288 dst dst."
, "nomor_meteran": "something"
, "tagihan":
    { "nomor_tagihan": "panjang"
    , "bulan": 12
    , "tahun": 2018
    , "minum": 420
    , "bayar": false
    , "tarif":
        [ { "mulai": 0
          , "sampai": 10
          , "harga": 0
          }
        , { "mulai": 11
          , "sampai": 20
          , "harga": 1000
          }
        , { "mulai": 21
          , "harga": 2000
          }
        ]
    }
}
```
### `daftar-tagihan-pelanggan.json`
```
{ "nomor_telepon": "6288 dst dst."
, "nomor_meteran": "something"
, "tagihan":
    [ { "nomor_tagihan": "panjang"
      , "bulan": 12
      , "tahun": 2018
      , "minum": 420
      , "bayar": false
      , "tarif":
          [ { "mulai": 0
            , "sampai": 10
            , "harga": 0
            }
          , { "mulai": 11
            , "sampai": 20
            , "harga": 1000
            }
          , { "mulai": 21
            , "harga": 2000
            }
          ]
      }
    [ { "nomor_tagihan": "panjang"
      , "bulan": 11
      , "minum": 420
      , "bayar": true
      , "tarif":
          [ { "mulai": 0
            , "sampai": 10
            , "harga": 0
            }
          , { "mulai": 11
            , "sampai": 20
            , "harga": 1000
            }
          , { "mulai": 21
            , "harga": 1000
            }
          ]
      }
    ]
}
```
### `tambah-air.json`
```
{ "nomor_meteran": "something"
, "minum_sampai": 420
}
```
### `air.json`
```
{ "bulan": 12
, "tahun": 2018
, "catat":
    { "nama_pemilik": "jaran"
    , "nomor_meteran": "something"
    , "alamat": 420
    , "minum_sampai": 420
    }
}
```
### `daftar-air.json`
```
[ { "tahun": 2018
  , "bulan": 12
  , "catat":
      [ { "nomor_meteran": "something"
        , "nama_pemilik": "john osterman"
        , "alamat": "alamat satu"
        , "minum_sampai": 420
        }
      , { "nomor_meteran": "another thing"
        , "nama_pemilik": "walter kovacs"
        , "alamat": "alamat dua"
        , "minum_sampai": 42
        }
      ]
, { "tahun": 2018
  , "bulan": 11
  , "catat":
      [ { "nomor_meteran": "something"
        , "nama_pemilik": "john osterman"
        , "alamat": "alamat satu"
        , "minum_sampai": 420
        }
      , { "nomor_meteran": "another thing"
        , "nama_pemilik": "walter kovacs"
        , "alamat": "alamat dua"
        , "minum_sampai": 42
        }
      ]
}
```
### `bayar-tagihan.json`
```
{ "nomor_tagihan": "something"
, "bayar": 420000
}
```
### `pelanggan.json`
```
{ "nama": "nama pelanggan"
, "nomor_telepon": "nomor-telepon"
, "alamat": "alamat rumah."
, "nomor_meteran": "nomor-meteran"
}
```
