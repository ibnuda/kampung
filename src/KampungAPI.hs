{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE KindSignatures  #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators   #-}
module KampungAPI where

import           Protolude

import           Control.Monad.Logger
import           Database.Persist.Postgresql
import           Network.Wai.Handler.Warp
import           Servant
import           Servant.Auth
import           Servant.Auth.Server

import           Conf
import           Model
import           Util

import           API.Bayaran
import           API.KeluarMasuk
import           API.Pelanggan
import           API.Petugas

type KampungAPI auth =
  KeluarMasukAPI
  :<|> (Auth auth Pengguna :> BayaranAPI)
  :<|> (Auth auth Pengguna :> PelangganAPI)
  :<|> (Auth auth Pengguna :> PetugasAPI)
  :<|> Raw

conduitProxy :: Proxy (KampungAPI '[JWT])
conduitProxy = Proxy

conduitServer :: Configuration -> Server (KampungAPI auth)
conduitServer conf =
  keluarMasukServer conf
    :<|> bayaranServer conf
    :<|> pelangganServer conf
    :<|> petugasServer conf
    :<|> serveDirectoryFileServer ""

connstring :: ByteString
connstring =
  "host=localhost "
    <> "port=5432 "
    <> "user=ibnu "
    <> "password=jaran "
    <> "dbname=kampung"

running :: IO ()
running = do
  jwk   <- readKey "../jaran.key.jwk"
  pool  <- runStderrLoggingT $ createPostgresqlPool connstring 10
  grups <- flip runSqlPool pool $ do
    doMigration
    selectList [] []
  let jws  = defaultJWTSettings jwk
      cfg  = defaultCookieSettings :. jws :. EmptyContext
      conf = Configuration pool jws grups
  run 8080 (serveWithContext conduitProxy cfg (conduitServer conf))
