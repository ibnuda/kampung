{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Util where

import           Protolude

import           Crypto.BCrypt
import           Crypto.JOSE
import           Data.Aeson
import qualified Data.ByteString.Char8 as BC (pack)
import qualified Data.ByteString.Lazy  as BL
import qualified Data.ByteString       as B
import           Data.Text             (unpack)
import           Data.Time.Clock
import           Database.Esqueleto as E
import           Servant.Auth.Server

import           Conf
import           Model
import           Model.Grouping
import           Types

generatePassword :: Text -> IO Text
generatePassword password = do
  mpass <- hashPasswordUsingPolicy slowerBcryptHashingPolicy $ BC.pack $ unpack
    password
  case mpass of
    Nothing -> generatePassword password
    Just pa -> return $ decodeUtf8 pa

generateToken
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Pengguna
  -> m Text
generateToken pengguna = do
  now    <- liftIO getCurrentTime
  jws    <- asks configurationJWTSettings
  etoken <- liftIO $ makeJWT pengguna jws (Just $ addUTCTime nominalDay now)
  case etoken of
    Left  _ -> throwError GagalMasuk
    Right y -> return . decodeUtf8 . BL.toStrict $ y

authresToMaybe :: AuthResult a -> Maybe a
authresToMaybe (Authenticated x) = Just x
authresToMaybe _                 = Nothing

encodeRespError :: Text -> BL.ByteString
encodeRespError = encode . RespError

penggunaKeRespDataPengguna :: GrupSistem -> Pengguna -> RespDataPengguna
penggunaKeRespDataPengguna g Pengguna {..} =
  RespDataPengguna penggunaNama penggunaNomorTelp penggunaAlamat g

entpenggunameteranminumKeRespMinumData
  :: (Entity Pengguna, Entity Meteran, Entity Minum) -> RespMinumData
entpenggunameteranminumKeRespMinumData (entpel, entmet, entminum) =
  let Pengguna {..} = entityVal entpel
      Meteran {..}  = entityVal entmet
      Minum {..}    = entityVal entminum
  in  RespMinumData penggunaNama meteranNomor penggunaAlamat minumSampai

rpmmKeRespPelMetMin :: (Pengguna, Meteran, [Entity Minum]) -> RespPelMetMin
rpmmKeRespPelMetMin (Pengguna {..}, Meteran {..}, entmins) =
  let f ((Entity _ m1):b@(Entity _ m2):xs) =
        RespMin (toInteger $ minumTahun m1)
                (minumBulan m1)
                (minumSampai m1)
                (minumSampai m1 - minumSampai m2)
          : f (b : xs)
      f [Entity _ Minum {..}] =
        [RespMin (toInteger minumTahun) minumBulan minumSampai minumSampai]
      f [] = f []
      x = f entmins
  in  RespPelMetMin penggunaNama penggunaNomorTelp meteranNomor x


tarifKeResTarif :: Tarif -> [RespTarifItem]
tarifKeResTarif Tarif {..} =
  let itemawal   = RespTarifItem 0 (Just tarifSampaiAwal) tarifHargaAwal
      itemtengah = RespTarifItem tarifSampaiAwal
                                 (Just tarifSampaiTengah)
                                 tarifHargaTengah
      itemakhir = RespTarifItem tarifSampaiTengah Nothing tarifHargaAkhir
  in  [itemawal, itemtengah, itemakhir]

riwayatminumToRespon
  :: ( E.Value (Maybe [Text])
     , E.Value (Maybe [Text])
     , E.Value (Maybe [Text])
     , E.Value (Maybe [Int64])
     , E.Value (Maybe [Int64])
     )
  -> RespRiwayatMinum
riwayatminumToRespon (Value mlnama, Value mlnotelp, Value mlnomet, Value mlmins, Value mlminl)
  = RespRiwayatMinum (forceText mlnama)
                     (forceText mlnotelp)
                     (forceText mlnomet)
                     (forceInt64 mlmins - forceInt64 mlminl)

forceText :: Maybe [Text] -> Text
forceText Nothing      = "data rusak."
forceText (Just []   ) = "data rusak."
forceText (Just (x:_)) = x
forceInt64 :: Num p => Maybe [p] -> p
forceInt64 Nothing      = 0
forceInt64 (Just []   ) = 0
forceInt64 (Just (x:_)) = x

daftartagihanToRespon
  :: ( Entity Pengguna
     , E.Value (Maybe [Text])
     , Entity Minum
     , Entity Tagihan
     , Entity Tarif
     , E.Value (Maybe [Int64])
     , E.Value (Maybe [Int64])
     )
  -> RespTagihanTahunBulan
daftartagihanToRespon (entp, vmn, entmin, enttag, enttar, vmbs, vmbl) =
  let Pengguna {..} = entityVal entp
      nomormet      = forceText $ unValue vmn
      mid           = fromSqlKey $ entityKey entmin
      Minum {..}    = entityVal entmin
      tid           = fromSqlKey $ entityKey enttag
      Tagihan {..}  = entityVal enttag
      tarif         = entityVal enttar
      minumsekarang = forceInt64 $ unValue vmbs
      minumlalu     = forceInt64 $ unValue vmbl
  in  RespTagihanTahunBulan penggunaNama
                            penggunaNomorTelp
                            nomormet
                            mid
                            tid
                            (tarifKeResTarif tarif)
                            (minumsekarang - minumlalu)

-- | "https://github.com/haskell-servant/servant-auth/pull/107/commits/3813a4d979dfbd47b6f9b667dfe163dd4743c141"
generateSecret :: MonadRandom m => m ByteString
generateSecret = getRandomBytes 256

fromSecret :: ByteString -> JWK
fromSecret = fromOctets

writeKey :: FilePath -> IO ()
writeKey filepath = B.writeFile filepath =<< generateSecret

readKey :: FilePath -> IO JWK
readKey filepath = fromSecret <$> B.readFile filepath
