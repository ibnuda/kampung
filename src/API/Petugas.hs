{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
module API.Petugas where

import           Protolude

import           Servant
import           Servant.Auth.Server

import           Conf
import           Model
import           Types

import           Handler.KelilingAir
import           Handler.KeluarMasuk

type PetugasAPI =
  "petugas"
    :> ReqBody '[ JSON] ReqGantiPass
    :> Put '[JSON] NoContent
  :<|> "petugas"
    :> "air"
    :> QueryParam "tahun" Integer
    :> QueryParam "bulan" Int
    :> Get '[ JSON] RespDaftarMinum
  :<|> "petugas"
    :> "air"
    :> ReqBody '[ JSON] ReqTambahAir
    :> Post '[ JSON] RespMinum
  :<|> "petugas"
    :> "air"
    :> ReqBody '[ JSON] ReqTambahAir
    :> Put '[ JSON] RespMinum

petugasProxy :: Proxy PetugasAPI
petugasProxy = Proxy

petugasApi :: MonadIO m => AuthResult Pengguna -> ServerT PetugasAPI (CoachT m)
petugasApi authres =
  putGantiPasswordHandler authres
    :<|> getRiwayatAirTahunBulanHandler authres
    :<|> postTambahAirHandler authres
    :<|> putUbahAirHandler authres

petugasServer :: Configuration -> AuthResult Pengguna -> Server PetugasAPI
petugasServer c a = hoistServer petugasProxy (coachToHandler c) (petugasApi a)
