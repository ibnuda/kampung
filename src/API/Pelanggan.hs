{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
module API.Pelanggan where

import           Protolude

import           Servant
import           Servant.Auth.Server

import           Conf
import           Model
import           Types

import           Handler.KeluarMasuk
import           Handler.Langganan

type PelangganAPI =
  "pelanggan"
    :> ReqBody '[ JSON] ReqGantiPass
    :> Put '[ JSON] NoContent
  :<|> "pelanggan"
    :> "air"
    :> Get '[ JSON] RespPelMetMin
  :<|> "pelanggan"
    :> "air"
    :> "detail"
    :> QueryParam "tahun" Integer
    :> QueryParam "bulan" Int
    :> Get '[ JSON] RespDataMinum

pelangganProxy :: Proxy PelangganAPI
pelangganProxy = Proxy

pelangganApi
  :: MonadIO m => AuthResult Pengguna -> ServerT PelangganAPI (CoachT m)
pelangganApi authres =
  putGantiPasswordHandler authres
    :<|> getRiwayatMinumSayaHandler authres
    :<|> getDataMinumTahunBulanHandler authres

pelangganServer :: Configuration -> AuthResult Pengguna -> Server PelangganAPI
pelangganServer conf authres =
  hoistServer pelangganProxy (coachToHandler conf) (pelangganApi authres)
