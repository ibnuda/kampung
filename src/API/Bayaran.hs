{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
module API.Bayaran where

import           Protolude

import           Servant
import           Servant.Auth.Server

import           Conf
import           Model
import           Types

import           Handler.Bayaran
import           Handler.KeluarMasuk

type BayaranAPI =
  "admin"
    :> ReqBody '[ JSON] ReqGantiPass
    :> Put '[JSON] NoContent
  :<|> "admin"
    :> "pelanggan"
    :> Capture "notelp" Text
    :> Get '[JSON] RespDataPengguna
  :<|> "admin"
    :> "air"
    :> QueryParam "tahun" Integer
    :> QueryParam "bulan" Int
    :> Get '[ JSON] RespRiwayatMinumTahunBulan
  :<|> "admin"
    :> "air"
    :> Capture "nomortelp" Text
    :> Get '[ JSON] RespPelMetMin
  :<|> "admin"
    :> "tagihan"
    :> QueryParam "tahun" Integer
    :> QueryParam "bulan" Int
    :> Get '[ JSON] [RespTagihanTahunBulan]

bayaranProxy :: Proxy BayaranAPI
bayaranProxy = Proxy

bayaranApi :: MonadIO m => AuthResult Pengguna -> ServerT BayaranAPI (CoachT m)
bayaranApi authres =
  putGantiPasswordHandler authres
    :<|> getPelangganHandler authres
    :<|> getDaftarMinumTahunBulanHandler authres
    :<|> getRiwayatMinumPelangganHandler authres
    :<|> getDaftarTagihanTahunBulanHandler authres

bayaranServer :: Configuration -> AuthResult Pengguna -> Server BayaranAPI
bayaranServer c a = hoistServer bayaranProxy (coachToHandler c) (bayaranApi a)
