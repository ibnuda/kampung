{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
module API.KeluarMasuk where

import           Protolude

import           Servant

import           Conf
import           Types

import           Handler.KeluarMasuk

type KeluarMasukAPI =
  "masuk"
    :> ReqBody '[ JSON] ReqLogin
    :> Post '[ JSON] RespPelangganToken

keluarMasukProxy :: Proxy KeluarMasukAPI
keluarMasukProxy = Proxy

keluarMasukApi :: MonadIO m => ServerT KeluarMasukAPI (CoachT m)
keluarMasukApi = postMasukHandler

keluarMasukServer :: Configuration -> Server KeluarMasukAPI
keluarMasukServer c =
  hoistServer keluarMasukProxy (coachToHandler c) keluarMasukApi
