{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
module Qu.Tarif where

import           Protolude          hiding (from, get, on, (<&>))

import           Database.Esqueleto

import           Model

selectTarifTerbaru
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => ReaderT backend m [Entity Tarif]
selectTarifTerbaru = do
  select $ from $ \tarif -> do
    orderBy [desc (tarif ^. TarifId)]
    limit 1
    return tarif

insertTarif
  :: (BaseBackend backend ~ SqlBackend, PersistStoreWrite backend, MonadIO m)
  => Int64 -- ^ Harga awal.
  -> Int64 -- ^ Batas awal.
  -> Int64 -- ^ Harga pertengahan.
  -> Int64 -- ^ Batas pertengahan.
  -> Int64 -- ^ Harga akhir.
  -> ReaderT backend m (Key Tarif)
insertTarif hawal sawal hteng steng hakhir =
  insert $ Tarif hawal sawal hteng steng hakhir
