{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE TypeFamilies     #-}
module Qu.Minum where

import           Protolude                   hiding (from, get, isNothing, on,
                                              (<&>))

import           Data.Time
import           Database.Esqueleto
import           Database.Esqueleto.PostgreSQL

import           Model

import           Qu.Umum

insertMinum
  :: ( BaseBackend backend ~ SqlBackend
     , MonadIO m
     , BackendCompatible SqlBackend backend
     , PersistQueryRead backend
     , PersistUniqueRead backend
     , PersistStoreWrite backend
     )
  => Text -- ^ Nomor meteran.
  -> Text -- ^ Nomor telepon Petugas.
  -> Integer -- ^ Tahun pencatatan.
  -> Int -- ^ Bulan pencatatn.
  -> Int64 -- ^ Nilai di meteran saat pencatatan.
  -> ReaderT backend m (Key Minum)
insertMinum nomormeteran teleponpetugas tahun bulan sampai = do
  meterpetid <- select $ from $ \(meteran, petugas) -> do
    where_ $ meteran ^. MeteranNomor ==. val nomormeteran
    where_ $ petugas ^. PenggunaNomorTelp ==. val teleponpetugas
    limit 1
    return (meteran ^. MeteranId, petugas ^. PenggunaId)
  case meterpetid of
    [] -> panic "Seharusnya ini tidak mungkin terjadi"
    (Value mid, Value pid):_ ->
      insert $ Minum mid pid (fromInteger tahun) bulan sampai

updateMinum
  :: MonadIO m
  => Key Minum -- ^ Primary key tabel minum.
  -> Int64 -- ^ Minum sampai bulan ini.
  -> ReaderT SqlBackend m ()
updateMinum minumid sampai = do
  update $ \minum -> do
    set minum [MinumSampai =. val sampai]
    where_ $ minum ^. MinumId ==. val minumid

selectMinumBulanIni
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Day
  -> ReaderT backend m [Entity Minum]
selectMinumBulanIni nomormeteran tanggalan = do
  let (tahun, bulan, _) = toGregorian tanggalan
  select $ from $ \(meteran `InnerJoin` minum) -> do
    on $ minum ^. MinumMeteranId ==. meteran ^. MeteranId
    where_ $ meteran ^. MeteranNomor ==. val nomormeteran
    where_ $ minum ^. MinumTahun ==. val (fromInteger tahun)
    where_ $ minum ^. MinumBulan ==. val bulan
    return minum

selectMinum
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Integer
  -> Int
  -> ReaderT
       backend
       m
       [(Entity Pengguna, Entity Meteran, Entity Minum)]
selectMinum tahun bulan = do
  select $ from $ \(minum `InnerJoin` meteran `InnerJoin` pengguna) -> do
    on $ pengguna ^. PenggunaId ==. meteran ^. MeteranPenggunaId
    on $ meteran ^. MeteranId ==. minum ^. MinumMeteranId
    where_ $ minum ^. MinumTahun ==. val (fromInteger tahun)
    where_ $ minum ^. MinumBulan ==. val bulan
    return (pengguna, meteran, minum)

selectMinumSatu
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Key Minum -- ^ Primary key minum.
  -> Maybe Integer -- ^ Tahun catat. Opsional.
  -> Maybe Int -- ^ Bulan catat. Opsional.
  -> ReaderT backend m [(Entity Pengguna, Entity Meteran, Entity Minum)] -- ^ Anu.
selectMinumSatu mid mtahun mbulan = do
  select $ from $ \(minum `InnerJoin` meteran `InnerJoin` pengguna) -> do
    on $ pengguna ^. PenggunaId ==. meteran ^. MeteranPenggunaId
    on $ meteran ^. MeteranId ==. minum ^. MinumMeteranId
    where_ $ minum ^. MinumId ==. val mid
    whereMaybe minum MinumTahun $ fromInteger <$> mtahun
    whereMaybe minum MinumBulan mbulan
    orderBy [desc (minum ^. MinumId)]
    limit 1
    return (pengguna, meteran, minum)

selectMinumByMeteran
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Key Meteran
  -> ReaderT backend m [Entity Minum]
selectMinumByMeteran mid = do
  select $ from $ \(meteran `InnerJoin` minum) -> do
    on $ meteran ^. MeteranId ==. minum ^. MinumMeteranId
    where_ $ meteran ^. MeteranId ==. val mid
    orderBy [desc (minum ^. MinumId)]
    return minum

selectMinumByMeteranTahunBulan mid ts bs tl bl = do
  select $ from $ \(minum `InnerJoin` meteran) -> do
    on $ minum ^. MinumMeteranId ==. meteran ^. MeteranId
    where_ $ meteran ^. MeteranId ==. val mid
    where_ $ minum ^. MinumTahun ==. val ts
    where_ $ minum ^. MinumBulan ==. val bs
    let minumsekarang = minumDiBulan (val mid) (val ts) (val bs)
        minumlalu = minumDiBulan (val mid) (val tl) (val bl)
    limit 1
    return $ (minum, minumsekarang, minumlalu)

-- | select array_agg(nama), array_agg(nomor_telp), array_agg(nomor), minumbulanini, minumbulanlalu
--     from pengguna
--          inner join meteran on pengguna.id = meteran.pengguna_id
--          left outer join minum on meteran.id = minum.meteran_id
--    where bulan = 7 and tahun = 2018 group by tahun, bulan;
selectMinumByTahunBulan
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Int64 -- ^ Tahun meteran.
  -> Int -- ^ Bulan meteran.
  -> ReaderT
       backend
       m
       [ ( Value (Maybe [Text])
         , Value (Maybe [Text])
         , Value (Maybe [Text])
         , Value (Maybe [Int64])
         , Value (Maybe [Int64])
         )
       ] -- ^ Agg. nama, telp, no. meteran, air bulan ini, bulan lalu, tahun, dan bulan.
selectMinumByTahunBulan tahun bulan = do
  let (tahunlalu, bulanlalu, _) =
        toGregorian $ addDays (-1) $ fromGregorian (toInteger tahun) bulan 1
  select $ from $ \(minum `InnerJoin` meteran `InnerJoin` pengguna) -> do
    on $ pengguna ^. PenggunaId ==. meteran ^. MeteranPenggunaId
    on $ minum ^. MinumMeteranId ==. meteran ^. MeteranId
    where_ $ minum ^. MinumTahun ==. val tahun
    where_ $ minum ^. MinumBulan ==. val bulan
    let minumbulanini =
          minumDiBulan (meteran ^. MeteranId) (val tahun) (val bulan)
        minumbulanlalu = minumDiBulan (meteran ^. MeteranId)
                                      (val $ fromInteger tahunlalu)
                                      (val bulanlalu)
    groupBy $ minum ^. MinumTahun
    groupBy $ minum ^. MinumBulan
    groupBy $ minum ^. MinumId
    orderBy [desc (minum ^. MinumId)]
    return
      ( arrayAgg (pengguna ^. PenggunaNama)
      , arrayAgg (pengguna ^. PenggunaNomorTelp)
      , arrayAgg (meteran ^. MeteranNomor)
      , arrayAgg minumbulanini
      , arrayAgg minumbulanlalu
      )
