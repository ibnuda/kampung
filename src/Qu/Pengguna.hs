{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE TypeFamilies     #-}
module Qu.Pengguna where

import           Protolude          hiding (from, get, on, (<&>))

import           Database.Esqueleto

import           Model
import           Model.Grouping

import           Util

import           Qu.Umum

insertGrup :: MonadIO m => GrupSistem -> ReaderT SqlBackend m (Key Grup)
insertGrup x = insert $ Grup x

updatePenggunaPassword :: MonadIO m => Text -> Text -> ReaderT SqlBackend m ()
updatePenggunaPassword nomortelepon passwordbaru = do
  password <- liftIO $ generatePassword passwordbaru
  update $ \pengguna -> do
    set pengguna [PenggunaPassword =. val password]
    where_ $ pengguna ^. PenggunaNomorTelp ==. val nomortelepon

selectPelangganByNomorTelepon
  :: MonadIO m => Text -> ReaderT SqlBackend m (Maybe (Entity Pengguna))
selectPelangganByNomorTelepon = getBy . UniqueNomorTelp

insertPengguna
  :: ( BaseBackend backend ~ SqlBackend
     , PersistStoreWrite backend
     , PersistUniqueRead backend
     , MonadIO m
     )
  => Text -- ^ Nama pengguna.
  -> Text -- ^ Nomor telepon pengguna.
  -> Text -- ^ Password pengguna.
  -> GrupSistem -- ^ Grup pengguna.
  -> Text -- ^ Alamat pengguna.
  -> ReaderT backend m (Entity Pengguna) -- ^ Pengguna.
insertPengguna n t p g a = do
  Just (Entity i _) <- getBy $ UniqueGrup g
  insertEntity $ Pengguna n t p i a

selectPenggunaByGrup
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Maybe (Key Pengguna)
  -> GrupSistem
  -> ReaderT backend m [Entity Pengguna]
selectPenggunaByGrup mid namagrup = do
  select $ from $ \(pengguna `InnerJoin` grup) -> do
    on $ pengguna ^. PenggunaGrupId ==. grup ^. GrupId
    where_ $ grup ^. GrupNama ==. val namagrup
    whereMaybe pengguna PenggunaId mid
    limitMaybe mid
    return pengguna

selectPengguna
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Key Pengguna
  -> ReaderT backend m [(Entity Pengguna, Value GrupSistem)]
selectPengguna pid = do
  select $ from $ \(pengguna `InnerJoin` grup) -> do
    on $ pengguna ^. PenggunaGrupId ==. grup ^. GrupId
    where_ $ pengguna ^. PenggunaId ==. val pid
    return (pengguna, grup ^. GrupNama)

updatePengguna
  :: MonadIO m
  => Key Pengguna
  -> Maybe Text
  -> Maybe Text
  -> Maybe GrupSistem
  -> ReaderT SqlBackend m ()
updatePengguna pid mnama malamat mnamagrup = do
  update $ \pengguna -> do
    set
      pengguna
      [ updateNotNullableByMaybe pengguna PenggunaNama   mnama
      , updateNotNullableByMaybe pengguna PenggunaAlamat malamat
      , updateNotNullableByMaybeFromAnotherTable pengguna
                                                 PenggunaGrupId
                                                 GrupId
                                                 GrupNama
                                                 mnamagrup
      ]
    where_ $ pengguna ^. PenggunaId ==. val pid
