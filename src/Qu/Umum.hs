{-# LANGUAGE MonoLocalBinds   #-}
{-# LANGUAGE FlexibleContexts #-}
module Qu.Umum where

import           Protolude                            hiding (from, get,
                                                       isNothing, on, (<&>))

import           Database.Esqueleto
import           Database.Esqueleto.Internal.Language

import           Model

nullTerisi
  :: (Esqueleto query expr backend, PersistField typ)
  => Bool
  -> expr (Value (Maybe typ))
  -> expr (Value Bool)
nullTerisi True  x = nothing !=. x
nullTerisi False x = nothing ==. x

whereMaybe
  :: (PersistField typ, Esqueleto query expr backend, PersistEntity val)
  => expr (Entity val)
  -> EntityField val typ
  -> Maybe typ
  -> query ()
whereMaybe ent acc (Just x) = where_ $ ent ^. acc ==. val x
whereMaybe _   _   Nothing  = where_ $ val True

updateNotNullableByMaybe
  :: (PersistField typ, Esqueleto query expr backend, PersistEntity val)
  => expr (Entity val) -- ^ Tabel A.
  -> EntityField val typ -- ^ Kolom X, `NOT NULL`.
  -> Maybe typ -- ^ Nilai kolom X.
  -> expr (Update val)
updateNotNullableByMaybe ent acc Nothing  = acc =. ent ^. acc
updateNotNullableByMaybe _   acc (Just x) = acc =. val x

limitMaybe :: Esqueleto query expr backend => Maybe a -> query ()
limitMaybe (Just _) = limit 1
limitMaybe Nothing  = return ()

updateNotNullableByMaybeFromAnotherTable
  :: ( PersistField typ2
     , PersistField typ1
     , From query expr backend (expr (Entity val1))
     , PersistEntity val2
     , PersistEntity val1
     )
  => expr (Entity val2) -- ^ Tabel A.
  -> EntityField val2 typ2 -- ^ Kolom X di tabel A.
  -> EntityField val1 typ2 -- ^ Kolom x di tabel B.
  -> EntityField val1 typ1 -- ^ KOlom y di tabel B.
  -> Maybe typ1 -- ^ Nilai kolom y baru.
  -> expr (Update val2)
updateNotNullableByMaybeFromAnotherTable _ acca idb accb (Just v) = do
  let anu = sub_select $ from $ \entb -> do
        where_ $ entb ^. accb ==. val v
        return $ entb ^. idb
  acca =. anu
updateNotNullableByMaybeFromAnotherTable enta acca _ _ Nothing = do
  acca =. enta ^. acca

minumDiBulan
  :: From query SqlExpr backend (SqlExpr (Entity Minum))
  => SqlExpr (Value (Key Meteran))
  -> SqlExpr (Value Int64)
  -> SqlExpr (Value Int)
  -> SqlExpr (Value Int64)
minumDiBulan mid t b = case_
  [ when_
      ( exists $ from $ \m -> do
        where_ $ m ^. MinumTahun ==. t
        where_ $ m ^. MinumBulan ==. b
        where_ $ m ^. MinumMeteranId ==. mid
      )
      then_
      ( sub_select $ from $ \m -> do
        where_ $ m ^. MinumMeteranId ==. mid
        where_ $ m ^. MinumTahun ==. t
        where_ $ m ^. MinumBulan ==. b
        return $ m ^. MinumSampai
      )
  ]
  (else_ $ val 0)
