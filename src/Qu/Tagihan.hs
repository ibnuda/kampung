{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
module Qu.Tagihan where

import           Protolude          hiding (from, get, isNothing, on, (<&>))

import           Data.Time
import           Database.Esqueleto
import           Database.Esqueleto.PostgreSQL

import           Model

import           Qu.Umum

selectTagihanPelangganX
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Bool
  -> ReaderT backend m [()]
selectTagihanPelangganX telepon sudahbayar = do
  select $ from $ \(pengguna `InnerJoin` meteran `InnerJoin` minum `InnerJoin` tagihan) ->
    do
      on $ tagihan ^. TagihanMinumId ==. minum ^. MinumId
      on $ minum ^. MinumMeteranId ==. meteran ^. MeteranId
      on $ meteran ^. MeteranPenggunaId ==. pengguna ^. PenggunaId
      where_ $ pengguna ^. PenggunaNomorTelp ==. val telepon
      where_ $ nullTerisi sudahbayar $ tagihan ^. TagihanTanggalBayar

insertTagihan
  :: (BaseBackend backend ~ SqlBackend, PersistStoreWrite backend, MonadIO m)
  => Key Minum
  -> Key Tarif
  -> ReaderT backend m (Key Tagihan)
insertTagihan minumid tarifid = insert $ Tagihan minumid tarifid Nothing

updateTagihan
  :: MonadIO m
  => Key Tagihan
  -> Maybe (Key Tarif)
  -> Maybe Day
  -> ReaderT SqlBackend m ()
updateTagihan tagihanid mtarifid mbayar = do
  update $ \tagihan -> do
    set
      tagihan
      [ updateNotNullableByMaybe tagihan TagihanTarifId mtarifid
      , TagihanTanggalBayar =. val mbayar
      ]
    where_ $ tagihan ^. TagihanId ==. val tagihanid

selectTagihanX
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Int64
  -> Int
  -> ReaderT
       backend
       m
       [ ( Entity Pengguna
         , Entity Meteran
         , Entity Minum
         , Entity Tagihan
         , Entity Tarif
         , Value Int64
         , Value Int64
         )
       ]
selectTagihanX tahun bulan = do
  let (tahunlalu, bulanlalu, _) =
        toGregorian $ addDays (-1) $ fromGregorian (toInteger tahun) bulan 1
  select
    $ from
    $ \(tagihan `InnerJoin` minum `InnerJoin` meteran `InnerJoin` pengguna `LeftOuterJoin` tarif) ->
        do
          on $ tagihan ^. TagihanTarifId ==. tarif ^. TarifId
          on $ meteran ^. MeteranPenggunaId ==. pengguna ^. PenggunaId
          on $ meteran ^. MeteranId ==. minum ^. MinumMeteranId
          on $ tagihan ^. TagihanMinumId ==. minum ^. MinumId
          let minumbulanini =
                minumDiBulan (meteran ^. MeteranId) (val tahun) (val bulan)
              minumbulanlalu = minumDiBulan (meteran ^. MeteranId)
                                            (val $ fromInteger tahunlalu)
                                            (val bulanlalu)
          where_ $ minum ^. MinumBulan ==. val bulan
          where_ $ minum ^. MinumTahun ==. val tahun
          return
            ( pengguna
            , meteran
            , minum
            , tagihan
            , tarif
            , minumbulanlalu
            , minumbulanini
            )

selectTagihanPelanggan notelp mtahun mbulan = do
  select
    $ from
    $ \(pengguna `InnerJoin` meteran `InnerJoin` minum `InnerJoin` tagihan `InnerJoin` tarif) ->
        do
          on $ tarif ^. TarifId ==. tagihan ^. TagihanTarifId
          on $ tagihan ^. TagihanMinumId ==. minum ^. MinumId
          on $ minum ^. MinumMeteranId ==. meteran ^. MeteranId
          on $ meteran ^. MeteranPenggunaId ==. pengguna ^. PenggunaId
          where_ $ pengguna ^. PenggunaNomorTelp ==. val notelp
          whereMaybe minum MinumTahun mtahun
          whereMaybe minum MinumBulan mbulan

selectTagihanByTahunBulan
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Integer
  -> Int
  -> ReaderT backend m [( Entity Pengguna
                        , Value (Maybe [Text])
                        , Entity Minum
                        , Entity Tagihan
                        , Entity Tarif
                        , Value (Maybe [Int64])
                        , Value (Maybe [Int64]))]
selectTagihanByTahunBulan tahun bulan = do
  let (tlalu, blalu, _) =
        toGregorian $ addDays (-1) $ fromGregorian tahun bulan 1
  select $ from $ \(pengguna
                    `InnerJoin` meteran
                    `InnerJoin` minum
                    `InnerJoin` tagihan
                    `InnerJoin` tarif) -> do
    on $ tagihan ^. TagihanTarifId ==. tarif ^. TarifId
    on $ minum ^. MinumId ==. tagihan ^. TagihanMinumId
    on $ meteran ^. MeteranId ==. minum ^. MinumMeteranId
    on $ pengguna ^. PenggunaId ==. meteran ^. MeteranPenggunaId
    where_ $ minum ^. MinumTahun ==. val (fromInteger tahun )
    where_ $ minum ^. MinumBulan ==. val bulan
    let minumbulanini = minumDiBulan (meteran ^. MeteranId)
                                     (val $ fromInteger tahun)
                                     (val bulan)
        minumbulanlalu = minumDiBulan (meteran ^. MeteranId)
                                      (val $ fromInteger tlalu)
                                      (val blalu)
    groupBy $ pengguna ^. PenggunaId
    groupBy $ minum ^. MinumId
    groupBy $ tarif ^. TarifId
    groupBy $ tagihan ^. TagihanId
    orderBy [asc (tagihan ^. TagihanId)]
    return
      ( pengguna
      , arrayAgg (meteran ^. MeteranNomor)
      , minum
      , tagihan
      , tarif
      , arrayAgg minumbulanini
      , arrayAgg minumbulanlalu
      )
