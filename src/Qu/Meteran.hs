{-# LANGUAGE FlexibleContexts #-}
module Qu.Meteran where

import           Protolude          hiding (from, get, on, (<&>))

import           Data.Time
import           Database.Esqueleto

import           Model

insertMeteran
  :: ( PersistUniqueWrite backend
     , PersistQueryWrite backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> Text
  -> ReaderT backend m ()
insertMeteran telepon nomor = do
  now <- liftIO getCurrentTime
  insertSelect $ from $ \pengguna -> do
    where_ $ pengguna ^. PenggunaNomorTelp ==. val telepon
    return
      $   Meteran
      <#  (pengguna ^. PenggunaId)
      <&> val nomor
      <&> (val $ utctDay now)
      <&> nothing

updateMeteran :: MonadIO m => Text -> ReaderT SqlBackend m ()
updateMeteran nomor = do
  now <- liftIO getCurrentTime
  update $ \meteran -> do
    set meteran [MeteranTanggalPutus =. just (val $ utctDay now)]
    where_ $ meteran ^. MeteranNomor ==. val nomor

selectMeteranByNomorTelp
  :: ( PersistUniqueRead backend
     , PersistQueryRead backend
     , BackendCompatible SqlBackend backend
     , MonadIO m
     )
  => Text
  -> ReaderT backend m [Entity Meteran]
selectMeteranByNomorTelp notelp = do
  select $ from $ \(pengguna `InnerJoin` meteran) -> do
    on $ pengguna ^. PenggunaId ==. meteran ^. MeteranPenggunaId
    where_ $ pengguna ^. PenggunaNomorTelp ==. val notelp
    return meteran
