{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Conf where

import           Protolude

import           Control.Monad.Trans.Reader hiding (asks)

import           Database.Persist.Postgresql
import           Servant
import           Servant.Auth.Server

import           Model
import           Types

data Configuration = Configuration
  { configurationPool           :: ConnectionPool
  , configurationJWTSettings    :: JWTSettings
  , configurationExistingGroups :: [Entity Grup]
  }

newtype CoachT m a = CoachT
  { runCoach :: ReaderT Configuration (ExceptT Gagal m) a
  } deriving ( Functor
             , Applicative
             , Monad
             , MonadReader Configuration
             , MonadError Gagal
             , MonadIO
             )

type Coach = CoachT IO

coachToHandler :: Configuration -> Coach a -> Handler a
coachToHandler conf coach = do
  let readerconf = runCoach coach
      servanterr = mapReaderT (withExceptT gagalToServantErr) readerconf
  Handler (runReaderT servanterr conf)

data Settings = Settings
  { settingsDatabaseName     :: Text
  , settingsDatabaseUsername :: Text
  , settingsDatabasePassword :: Text
  , settingsDatabasePort     :: Int
  , settingsDatabaseHost     :: Text
  }

runDb :: (MonadIO m, MonadReader Configuration m) => SqlPersistT IO b -> m b
runDb q = do
  pool <- asks configurationPool
  liftIO $ runSqlPool q pool
