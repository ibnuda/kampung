{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Handler.KelilingAir where

import           Protolude

import           Servant.Auth.Server

import           Data.Time

import           Conf
import           Model
import           Types
import           Util

import           Bisnis.Petugas

getRiwayatAirTahunBulanHandler
  :: MonadIO m
  => AuthResult Pengguna -- ^ Hasil parse jwt.
  -> Maybe Integer -- ^ Param tahun, opsional.
  -> Maybe Int -- ^ Param bulan, opsional.
  -> CoachT m RespDaftarMinum
getRiwayatAirTahunBulanHandler (Authenticated petugas) mtahun mbulan = do
  (tahun, bulan, _) <- toGregorian . utctDay <$> liftIO getCurrentTime
  daftarminum       <- lihatMinumTahunBulan petugas mtahun mbulan
  return $ RespDaftarMinum (fromInteger tahun) bulan $ map
    entpenggunameteranminumKeRespMinumData
    daftarminum
getRiwayatAirTahunBulanHandler _ _ _ =
  throwError $ GagalTakBerwenang "Tidak boleh lihat sini."

postTambahAirHandler
  :: MonadIO m => AuthResult Pengguna -> ReqTambahAir -> CoachT m RespMinum
postTambahAirHandler (Authenticated petugas) ReqTambahAir {..} = do
  (mid, _) <- catatMeteran petugas
                           reqtambahairNomorMeteran
                           reqtambahairMinumSampai
  (tahun, bulan, _) <- toGregorian . utctDay <$> liftIO getCurrentTime
  minum             <- lihatMinumSatu petugas mid
  case minum of
    [] -> throwError $ GagalMinumNil reqtambahairNomorMeteran
    x:_ ->
      return $ RespMinum tahun bulan $ entpenggunameteranminumKeRespMinumData x
postTambahAirHandler _ _ =
  throwError $ GagalTakBerwenang "Tidak boleh mengisi."

putUbahAirHandler
  :: MonadIO m => AuthResult Pengguna -> ReqTambahAir -> CoachT m RespMinum
putUbahAirHandler (Authenticated petugas) ReqTambahAir {..} = do
  minum <- ubahMeteran petugas reqtambahairNomorMeteran reqtambahairMinumSampai
  (tahun, bulan, _) <- toGregorian . utctDay <$> liftIO getCurrentTime
  case minum of
    [] -> throwError $ GagalMinumNil reqtambahairNomorMeteran
    x:_ ->
      return $ RespMinum tahun bulan $ entpenggunameteranminumKeRespMinumData x
putUbahAirHandler _ _ = throwError $ GagalTakBerwenang "Tidak boleh mengubah."
