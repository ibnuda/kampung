{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Handler.Bayaran where

import           Protolude

import           Database.Esqueleto

import           Servant.Auth.Server

import           Conf
import           Model
import           Model.Grouping
import           Types

import           Bisnis.Administrasi

import           Util

getPelangganHandler
  :: MonadIO m
  => AuthResult Pengguna -- ^ Yang meminta.
  -> Text -- ^ Nomor telepon pelanggan.
  -> CoachT m RespDataPengguna -- ^ Data pengguna.
getPelangganHandler (Authenticated admin) notelp = do
  (Entity _ p, g) <- lihatPengguna admin notelp
  return $ penggunaKeRespDataPengguna g p
getPelangganHandler _ _ = throwError $ GagalTakBerwenang "Mbayar."

postTambahPelangganHandler
  :: MonadIO m
  => AuthResult Pengguna -- ^ Yang meminta.
  -> ReqPelBaru -- ^ Payload.
  -> CoachT m RespDataPengguna -- ^ Berhasil.
postTambahPelangganHandler (Authenticated admin) ReqPelBaru {..} = do
  (Entity _ pelanggan) <- tambahPelanggan admin
                                          reqpbNama
                                          reqpbNomorTelepon
                                          reqpbPassword
                                          reqpbAlamat
  return $ penggunaKeRespDataPengguna Pelanggan pelanggan
postTambahPelangganHandler _ _ =
  throwError $ GagalTakBerwenang "Nambah Pelanggan."

getRiwayatMinumPelangganHandler
  :: MonadIO m
  => AuthResult Pengguna -- ^ Yang meminta.
  -> Text -- ^ Nomor telepon pelanggan.
  -> CoachT m RespPelMetMin
getRiwayatMinumPelangganHandler (Authenticated admin) notelp = do
  datapelanggan <- lihatRiwayatMinumPelanggan admin notelp
  return $ rpmmKeRespPelMetMin datapelanggan
getRiwayatMinumPelangganHandler _ _ =
  throwError $ GagalTakBerwenang "Lihat riwayat pelanggan."

getDaftarMinumTahunBulanHandler
  :: MonadIO m
  => AuthResult Pengguna -- ^ Peminta.
  -> Maybe Integer -- ^ Tahun riwayat, opsional. Default tahun ini.
  -> Maybe Int -- ^ Bulan riwayat, opsional. Default bulan ini.
  -> CoachT m RespRiwayatMinumTahunBulan
getDaftarMinumTahunBulanHandler (Authenticated admin) mtahun mbulan = do
  (tahun, bulan, riwayat) <- lihatDaftarMinumTahunBulan admin mtahun mbulan
  return $ RespRiwayatMinumTahunBulan tahun bulan $ map riwayatminumToRespon
                                                        riwayat
getDaftarMinumTahunBulanHandler _ _ _ =
  throwError $ GagalTakBerwenang "Lihat riwayat minum bulan"

getDaftarTagihanTahunBulanHandler
  :: MonadIO m
  => AuthResult Pengguna
  -> Maybe Integer
  -> Maybe Int
  -> CoachT m [RespTagihanTahunBulan]
getDaftarTagihanTahunBulanHandler (Authenticated admin) mtahun mbulan = do
  anu <- lihatTagihanBulanTahun admin mtahun mbulan
  return $ map daftartagihanToRespon anu
getDaftarTagihanTahunBulanHandler _ _ _ =
  throwError $ GagalTakBerwenang "Lihat riwayat minum bulan"
