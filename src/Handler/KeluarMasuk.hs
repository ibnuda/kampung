{-# LANGUAGE RecordWildCards #-}
module Handler.KeluarMasuk where

import           Protolude

import           Database.Esqueleto
import           Servant
import           Servant.Auth.Server

import           Conf
import           Model
import           Types

import           Bisnis.Umum

postMasukHandler
  :: MonadIO m
  => ReqLogin -- ^ Nomor telp dan password.
  -> CoachT m RespPelangganToken -- ^ Nama, nomor telp, gid, dan token.
postMasukHandler ReqLogin {..} = do
  (Pengguna {..}, token) <- buatTokenLogin reqloginNomorTelepon reqloginPassword
  return $ RespPelangganToken penggunaNama
                              penggunaNomorTelp
                              (fromSqlKey penggunaGrupId)
                              token

putGantiPasswordHandler
  :: MonadIO m
  => AuthResult Pengguna -- ^ Yang meminta.
  -> ReqGantiPass -- ^ pass lama dan baru.
  -> CoachT m NoContent -- ^ Nama, nomor telp, gid, dan token.
putGantiPasswordHandler (Authenticated pengguna) ReqGantiPass {..} = do
  gantiPassword pengguna rgpPasswordLama rgpPasswordBaru
  return NoContent
putGantiPasswordHandler _ _ =
  throwError $ GagalTakBerwenang "Tidak boleh ganti password"
