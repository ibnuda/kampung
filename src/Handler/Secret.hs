{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Handler.Secret where

import           Protolude

import           Database.Esqueleto

import           Servant
import           Servant.Auth.Server

import           Conf
import           Model
import           Model.Grouping
import           Types

import           Util

import           Bisnis.SuperAdmin
import           Bisnis.Umum

postTambahAdminHandler
  :: MonadIO m => AuthResult Pengguna -> ReqPelBaru -> CoachT m NoContent
postTambahAdminHandler (Authenticated admin) ReqPelBaru {..} = do
  _ <- cekKewenanganMinimal admin SuperAdmin
  _ <- tambahAdmin admin reqpbNama reqpbNomorTelepon reqpbPassword reqpbAlamat
  return NoContent
postTambahAdminHandler _ _ =
  throwError $ GagalTakBerwenang "Tidak boleh menambah admin."

getLihatSemuaAdminHandler
  :: MonadIO m => AuthResult Pengguna -> CoachT m [RespDataPengguna]
getLihatSemuaAdminHandler (Authenticated admin) = do
  pengguna <- lihatSemuaAdmin admin
  return $ map (penggunaKeRespDataPengguna Admin . entityVal) pengguna
getLihatSemuaAdminHandler _ =
  throwError $ GagalTakBerwenang "Tidak boleh melihat ini."
