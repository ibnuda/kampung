{-# LANGUAGE RecordWildCards #-}
module Handler.Langganan where

import           Protolude

import           Database.Esqueleto
import           Servant.Auth.Server

import           Conf
import           Model
import           Types
import           Util

import Bisnis.Umum
import           Bisnis.Pelanggan

getRiwayatMinumSayaHandler
  :: MonadIO m => AuthResult Pengguna -> CoachT m RespPelMetMin
getRiwayatMinumSayaHandler (Authenticated pengguna) = do
  riwayat <- cekMinum pengguna
  return $ rpmmKeRespPelMetMin riwayat
getRiwayatMinumSayaHandler _ =
  throwError $ GagalTakBerwenang "Lihat riwayat minum"

getDataMinumTahunBulanHandler
  :: MonadIO m
  => AuthResult Pengguna
  -> Maybe Integer
  -> Maybe Int
  -> CoachT m RespDataMinum
getDataMinumTahunBulanHandler (Authenticated Pengguna {..}) mtahun mbulan = do
  (Entity _ Meteran {..})       <- cekPenggunaPunyaMeteran penggunaNomorTelp
  (Entity _ Minum {..}, vs, vl) <- cekMinumTahunBulan penggunaNomorTelp
                                                      mtahun
                                                      mbulan
  return $ RespDataMinum penggunaNama penggunaNomorTelp meteranNomor $ RespMin
    (toInteger minumTahun)
    minumBulan
    minumSampai
    (unValue vs - unValue vl)
getDataMinumTahunBulanHandler _ _ _ =
  throwError $ GagalTakBerwenang "Lihat data minum"
