{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE KindSignatures   #-}
{-# LANGUAGE Rank2Types       #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TemplateHaskell  #-}
module Types where

import           Protolude

import           Data.Aeson
import           Data.Aeson.Casing
import qualified Data.ByteString.Lazy as BL
import           Data.Text            (pack)
import           Servant.Server

import           Model.Grouping

omitsnake :: Options
omitsnake =
  let snake = aesonPrefix snakeCase in snake { omitNothingFields = True }

data ReqPelBaru = ReqPelBaru
  { reqpbNama         :: Text -- ^ Nama pelanggan baru.
  , reqpbNomorTelepon :: Text -- ^ Nomor telepon pelanggan baru.
  , reqpbPassword     :: Text -- ^ Password.
  , reqpbAlamat       :: Text -- ^ Alamat.
  } deriving (Generic)
instance FromJSON ReqPelBaru where
  parseJSON = genericParseJSON omitsnake

data ReqLogin = ReqLogin
  { reqloginNomorTelepon :: Text
  , reqloginPassword     :: Text
  } deriving (Generic)
instance FromJSON ReqLogin where
  parseJSON = genericParseJSON omitsnake

data ReqGantiPass = ReqGantiPass
  { rgpPasswordLama :: Text
  , rgpPasswordBaru :: Text
  } deriving (Generic)
instance FromJSON ReqGantiPass where
  parseJSON = genericParseJSON omitsnake

data ReqTambahAir = ReqTambahAir
  { reqtambahairNomorMeteran :: Text
  , reqtambahairMinumSampai  :: Int64
  } deriving (Generic)
instance FromJSON ReqTambahAir where
  parseJSON = genericParseJSON omitsnake

data RespError = RespError
  { resperrErrors :: Text
  } deriving (Generic)
instance ToJSON RespError where
  toJSON = genericToJSON omitsnake

data RespDataPengguna = RespDataPengguna
  { resdatapengNama    :: Text
  , resdatapengTelepon :: Text
  , resdatapengAlamat  :: Text
  , resdatapengGrup    :: GrupSistem
  } deriving (Generic)
instance ToJSON RespDataPengguna where
  toJSON = genericToJSON omitsnake

data RespPelangganToken = RespPelangganToken
  { resppeltNama         :: Text
  , resppeltNomorTelepon :: Text
  , resppeltGroupId      :: Int64
  , resppeltToken        :: Text
  } deriving (Generic)
instance ToJSON RespPelangganToken where
  toJSON = genericToJSON omitsnake

data RespTarifItem = RespTarifItem
  { resptaritMulai  :: Int64
  , resptaritSampai :: Maybe Int64
  , resptaritHarga  :: Int64
  } deriving (Generic)
instance ToJSON RespTarifItem where
  toJSON = genericToJSON omitsnake

data RespTarif = RespTarif
  { resptarifTarif :: [RespTarifItem]
  } deriving (Generic)
instance ToJSON RespTarif where
  toJSON = genericToJSON omitsnake

data RespMinumData = RespMinumData
  { respmindataNamaPemilik  :: Text
  , respmindataNomorMeteran :: Text
  , respmindataAlamat       :: Text
  , respmindataMinumSampai  :: Int64
  } deriving (Generic)
instance ToJSON RespMinumData where
  toJSON = genericToJSON omitsnake

data RespMinum = RespMinum
  { respminumTahun :: Integer
  , respminumBulan :: Int
  , respminumCatat :: RespMinumData
  } deriving (Generic)
instance ToJSON RespMinum where
  toJSON = genericToJSON omitsnake

data RespDaftarMinum = RespDaftarMinum
  { respdaftminumTahun :: Integer
  , respdaftminumBulan :: Int
  , respdaftminumCatat :: [RespMinumData]
  } deriving (Generic)
instance ToJSON RespDaftarMinum where
  toJSON = genericToJSON omitsnake

data RespEmbuh = RespEmbuh
  { embuhNama :: Text
  , embuhTelp :: Text
  , embuhNoMe :: Text
  , embuhTahu :: Integer
  , embuhBula :: Int
  , embuhLalu :: Int64
  , embuhNaow :: Int64
  , embuhTari :: [RespTarifItem]
  } deriving (Generic)
instance ToJSON RespEmbuh where
  toJSON = genericToJSON omitsnake

data RespMin = RespMin
  { rmTahun  :: Integer
  , rmBulan  :: Int
  , rmSampai :: Int64
  , rmBanyak :: Int64
  } deriving (Generic)
instance ToJSON RespMin where
  toJSON = genericToJSON omitsnake

data RespPelMetMin = RespPelMetMin
  { rpmmNama         :: Text
  , rpmmNomorTelepon :: Text
  , rpmmNomorMeteran :: Text
  , rpmmMinum        :: [RespMin]
  } deriving (Generic)
instance ToJSON RespPelMetMin where
  toJSON = genericToJSON omitsnake

data RespDataMinum = RespDataMinum
  { rdmNama         :: Text
  , rdmNomorTelepon :: Text
  , rdmNomorMeteran :: Text
  , rdmMinum        :: RespMin
  } deriving (Generic)
instance ToJSON RespDataMinum where
  toJSON = genericToJSON omitsnake

data RespRiwayatMinum = RespRiwayatMinum
  { rrmNamaPelanggan :: Text
  , rmmNomorTelepon  :: Text
  , rmmNomorMeteran  :: Text
  , rmmMinum         :: Int64
  } deriving (Generic)
instance ToJSON RespRiwayatMinum where
  toJSON = genericToJSON omitsnake

data RespRiwayatMinumTahunBulan = RespRiwayatMinumTahunBulan
  { rmmttTahun   :: Integer
  , rmmttBulan   :: Int
  , rmmttRiwayat :: [RespRiwayatMinum]
  } deriving (Generic)
instance ToJSON RespRiwayatMinumTahunBulan where
  toJSON = genericToJSON omitsnake

data RespTagihanTahunBulan = RespTagihanTahunBulan
  { rttbNamaPelanggan :: Text
  , rttbTelpPelanggan :: Text
  , rttbNomorMeteran  :: Text
  , rdtbNomorMinum    :: Int64
  , rdtbNomorTagihan  :: Int64
  , rttbTarif         :: [RespTarifItem]
  , rttbMinum         :: Int64
  } deriving (Generic)
instance ToJSON RespTagihanTahunBulan where
  toJSON = genericToJSON omitsnake

data Gagal
  = GagalMasuk
  | GagalAdminNil { gadminnilPid :: Int64 }
  | GagalBayar { gbayarAlasan :: Text }
  | GagalCatatAir { gcatatairAlasan       :: Text
                  , gcatatairNomorMeteran :: Text }
  | GagalDB { gdbAlasan :: Text
            , gdbAksi   :: Text }
  | GagalDataTidakAda
  | GagalMeteranAda { gmeteranadaNomor :: Text }
  | GagalMeteranNil { gmeterannilNomor :: Text }
  | GagalMinumAda { gminumadaNomor :: Text }
  | GagalMinumNil { gminumnilNomor :: Text }
  | GagalPasswordBeda
  | GagalPenggunaAda { gpenggunaadaTelepon :: Text }
  | GagalPenggunaNil { gpenggunanilTelepon :: Text }
  | GagalPenggunaTunaMeteran
  | GagalTakBerwenang { gtakwenangSaat :: Text }
  | GagalTambahPelanggan { gtambahpelAlasan :: Text
                         , gtambahpelDetail :: Text }
  | GagalTanggalBelumAda { gtglbTahun :: Integer
                         , gtglbBulan :: Int }
  | GagalTanggalTidakValid { gtgltTahun :: Integer
                           , gtgltBulan :: Int }
  | GagalTanggalTidakAda
  | GagalTarifKosong
  | GagalUbahAir { gubahairAlasan       :: Text
                 , gubahairNomorMeteran :: Text }
  | GagalUpdatePassword { gupdatepassAlasan :: Text }
  deriving (Show, Eq)

encodeRespError' :: Text -> BL.ByteString
encodeRespError' = encode . RespError

gagalToServantErr :: Gagal -> ServantErr
gagalToServantErr GagalMasuk =
  err400 { errBody = encodeRespError' "nomor telepon atau password salah" }
gagalToServantErr (GagalAdminNil n) = err404
  { errBody = encodeRespError'
    $  "nomor "
    <> (pack . show $ n)
    <> " bukan admin."
  }
gagalToServantErr (GagalBayar y) = err404 { errBody = encodeRespError' y }
gagalToServantErr (GagalCatatAir a n) =
  err404 { errBody = encodeRespError' (a <> " di nomor " <> n) }
gagalToServantErr (GagalDB a x) =
  err500 { errBody = encodeRespError' (a <> " saat " <> x) }
gagalToServantErr GagalDataTidakAda =
  err404 { errBody = encodeRespError' "Data kosong." }
gagalToServantErr (GagalMeteranAda n) =
  err422 { errBody = encodeRespError' ("nomor " <> n <> " sudah dipakai.") }
gagalToServantErr (GagalMeteranNil n) =
  err404 { errBody = encodeRespError' ("nomor " <> n <> " tidak dipakai.") }
gagalToServantErr (GagalMinumAda n) =
  err422 { errBody = encodeRespError' $ n <> " sudah dicatat." }
gagalToServantErr (GagalMinumNil n) =
  err404 { errBody = encodeRespError' $ n <> " belum dicatat." }
gagalToServantErr (GagalPenggunaAda n) =
  err422 { errBody = encodeRespError' ("nomor " <> n <> " sudah dipakai.") }
gagalToServantErr (GagalPenggunaNil n) =
  err404 { errBody = encodeRespError' ("nomor " <> n <> " tidak dipakai.") }
gagalToServantErr (GagalTakBerwenang saat) =
  err401 { errBody = encodeRespError' saat }
gagalToServantErr (GagalTambahPelanggan a d) =
  err401 { errBody = encodeRespError' (a <> "saat " <> d) }
gagalToServantErr GagalTanggalTidakAda =
  err400{ errBody = encodeRespError' "harap isi tanggal." }
gagalToServantErr (GagalTanggalBelumAda t b) = err400
  { errBody = encodeRespError'
    $  (pack . show $ t)
    <> " "
    <> (pack . show $ b)
    <> " belum terjadi."
  }
gagalToServantErr (GagalTanggalTidakValid t b) = err400
  { errBody = encodeRespError'
    $  (pack . show $ t)
    <> " "
    <> (pack . show $ b)
    <> " tidak valid."
  }
gagalToServantErr (GagalUbahAir a n) =
  err422 { errBody = encodeRespError' (a <> " di nomor " <> n) }
gagalToServantErr (GagalUpdatePassword a) =
  err400 { errBody = encodeRespError' a }
gagalToServantErr GagalPasswordBeda =
  err401 { errBody = encodeRespError' "Password beda." }
gagalToServantErr GagalPenggunaTunaMeteran =
  err404 { errBody = encodeRespError' "Ybs tidak punya meteran." }
gagalToServantErr GagalTarifKosong =
  err404 { errBody = encodeRespError' "Tarif belum ditentukan." }
gagalToServantErr _ = err500 { errBody = encodeRespError' "Mohon bilang ke Ibnu."}

instance Exception Gagal
