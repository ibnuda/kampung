{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Bisnis.Administrasi where

import           Protolude

import           Database.Esqueleto

import           Conf
import           Model
import           Model.Grouping
import           Types

import           Bisnis.Umum

import           Qu.Minum
import           Qu.Tagihan

tambahPelanggan
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Yang meminta.
  -> Text -- ^ Nama.
  -> Text -- ^ Telepon.
  -> Text -- ^ Password.
  -> Text -- ^ Alamat.
  -> m (Entity Pengguna)
tambahPelanggan = tambahPengguna Pelanggan

tambahPetugas
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Yang meminta.
  -> Text -- ^ Nama.
  -> Text -- ^ Telepon.
  -> Text -- ^ Password.
  -> Text -- ^ Alamat.
  -> m (Entity Pengguna)
tambahPetugas = tambahPengguna Petugas

lihatPengguna
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna
  -> Text
  -> m (Entity Pengguna, GrupSistem)
lihatPengguna admin notelp = do
  _        <- cekKewenanganMinimal admin Admin
  mp       <- runDb $ getBy $ UniqueNomorTelp notelp
  grup     <- asks configurationExistingGroups
  pengguna <- case mp of
    Nothing -> throwError $ GagalPenggunaNil notelp
    Just x  -> return x
  namagrup <-
    case
      find (\x -> (penggunaGrupId $ entityVal pengguna) == entityKey x) grup
    of
      Nothing -> throwError $ GagalDB "Grup Tidak Ada." "Cek Grup."
      Just (Entity _ (Grup y)) -> return y
  return (pengguna, namagrup)

lihatRiwayatMinumPelanggan
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna
  -> Text
  -> m (Pengguna, Meteran, [Entity Minum])
lihatRiwayatMinumPelanggan admin nomortelepon = do
  _ <- cekKewenanganMinimal admin Admin
  lihatRiwayatMinumByNomorTelepon nomortelepon

lihatDaftarMinumTahunBulan
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Yang Meminta.
  -> Maybe Integer -- ^ Tahun.
  -> Maybe Int -- ^ Bulan.
  -> m
       ( Integer
       , Int
       , [ ( Value (Maybe [Text])
           , Value (Maybe [Text])
           , Value (Maybe [Text])
           , Value (Maybe [Int64])
           , Value (Maybe [Int64])
           )
         ]
       ) -- ^ Tahun, bulan, dan daftar nama, no. telp, no. meteran, bulan lalu, dan bulan ini.
lihatDaftarMinumTahunBulan admin mtahun mbulan = do
  _              <- cekKewenanganMinimal admin Admin
  (tahun, bulan) <- cekTahunBulanValid mtahun mbulan
  riwayat        <- runDb $ selectMinumByTahunBulan (fromInteger tahun) bulan
  return (tahun, bulan, riwayat)

-- | Lihat tagihan di tahun dan bulan tertentu.
--   Return: `penggunaNama`
--           `penggunaNomorTelp`
--           `meteranNomor`
--           `Key Minum`
--           `Key Tagihan`
--           `tagihanTanggalBayar`
--           `tarifHargaAwal`
--           `tarifSampaiAwal`
--           `tarifHargaTengah`
--           `tarifSampaiTengah`
--           `tarifHargaAkhir`
--           `minumDiBulan`
--           `minumDiBulan`
lihatTagihanBulanTahun
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Yang meminta.
  -> Maybe Integer -- ^ Tahun, opsional.
  -> Maybe Int -- ^ Bulan, opsional.
  -> m
       [ ( Entity Pengguna
         , Value (Maybe [Text])
         , Entity Minum
         , Entity Tagihan
         , Entity Tarif
         , Value (Maybe [Int64])
         , Value (Maybe [Int64])
         )
       ]
lihatTagihanBulanTahun admin mtahun mbulan = do
  _              <- cekKewenanganMinimal admin Admin
  (tahun, bulan) <- cekTahunBulanValid mtahun mbulan
  runDb $ selectTagihanByTahunBulan tahun bulan
