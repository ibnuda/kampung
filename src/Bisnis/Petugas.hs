{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Bisnis.Petugas where

import           Protolude

import           Database.Esqueleto
import           Data.Time.Clock
import           Data.Time

import           Conf
import           Model
import           Model.Grouping
import           Types

import           Qu.Minum
import           Qu.Tagihan

import           Bisnis.Umum

catatMeteran
  :: (MonadReader Configuration m, MonadError Gagal m, MonadIO m)
  => Pengguna -- ^ Petugas.
  -> Text -- ^ Nomor meteran.
  -> Int64 -- ^ Penggunaan sampai pencatatan.
  -> m (Key Minum, Key Tagihan)
catatMeteran petugas nomormeteran minum = do
  (bulan, tanggal, _) <- toGregorian . utctDay <$> liftIO getCurrentTime
  _                   <- cekKewenanganMinimal petugas Petugas
  _                   <- cekMeteranAda nomormeteran
  _                   <- cekMinumNil nomormeteran
  (Entity tid _)      <- cekTarifTerbaru
  runDb $ do
    mid <- insertMinum nomormeteran
                       (penggunaNomorTelp petugas)
                       bulan
                       tanggal
                       minum
    tagid <- insertTagihan mid tid
    return (mid, tagid)

ubahMeteran
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Petugas.
  -> Text -- ^ Nomor meteran.
  -> Int64 -- ^ Penggunaan sampai pencatatan.
  -> m [(Entity Pengguna, Entity Meteran, Entity Minum)]
ubahMeteran petugas nomormeteran sampai = do
  _            <- cekKewenanganMinimal petugas Petugas
  (Entity k _) <- cekMinumAda nomormeteran
  runDb $ do
    updateMinum k sampai
    selectMinumSatu k Nothing Nothing

lihatMinumTahunBulan
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna
  -> Maybe Integer
  -> Maybe Int
  -> m [(Entity Pengguna, Entity Meteran, Entity Minum)]
lihatMinumTahunBulan petugas mtahun mbulan = do
  (tahun, bulan) <- cekTahunBulanValid mtahun mbulan
  _              <- cekKewenanganMinimal petugas Petugas
  runDb $ selectMinum tahun bulan

lihatMinumSatu
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna
  -> Key Minum
  -> m [(Entity Pengguna, Entity Meteran, Entity Minum)]
lihatMinumSatu petugas mid = do
  _ <- cekKewenanganMinimal petugas Petugas
  runDb $ selectMinumSatu mid Nothing Nothing
