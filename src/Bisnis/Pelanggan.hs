{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Bisnis.Pelanggan where

import           Protolude

import           Database.Esqueleto

import           Conf
import           Model
import           Types

import           Qu.Minum

import           Bisnis.Umum

cekTagihan Pengguna {..} = panic ""

cekMinum
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Pengguna
  -> m (Pengguna, Meteran, [Entity Minum])
cekMinum Pengguna {..} = lihatRiwayatMinumByNomorTelepon penggunaNomorTelp

cekMinumTahunBulan
  :: (MonadReader Configuration m, MonadIO m, MonadError Gagal m)
  => Text -- ^ Nomor Telepon.
  -> Maybe Integer -- ^ Tahun. Opsional.
  -> Maybe Int -- ^.Bulan. Opsional.
  -> m (Entity Minum, Value Int64, Value Int64)
cekMinumTahunBulan notelp mtahun mtanggal = do
  (tahun, bulan) <- cekTahunBulanValid mtahun mtanggal
  (tlalu, blalu) <- bulanLalu tahun bulan
  Entity mid _   <- cekPenggunaPunyaMeteran notelp
  pmm            <- runDb $ selectMinumByMeteranTahunBulan mid
                                                           (fromInteger tahun)
                                                           bulan
                                                           (fromInteger tlalu)
                                                           blalu
  case pmm of
    []  -> throwError GagalDataTidakAda
    x:_ -> return x
