{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Bisnis.SuperAdmin where

import           Protolude

import           Database.Esqueleto

import           Conf
import           Model
import           Model.Grouping
import           Types

import           Bisnis.Umum

import           Qu.Pengguna

tambahAdmin
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Yang meminta.
  -> Text -- ^ Nama
  -> Text -- ^ Telepon
  -> Text -- ^ Password
  -> Text -- ^ Alamat
  -> m (Entity Pengguna)
tambahAdmin = tambahPengguna Admin

lihatSemuaAdmin
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna
  -> m [Entity Pengguna]
lihatSemuaAdmin = lihatPenggunaByGrup Admin Nothing

lihatAdmin
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Pengguna
  -> Key Pengguna
  -> m (Pengguna, GrupSistem)
lihatAdmin requester admid = do
  pengguna <- lihatPenggunaDanGrup requester admid
  case pengguna of
    []                      -> throwError $ GagalPenggunaNil ""
    (Entity _ p, Value g):_ -> return (p, g)

angkatAdmin
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna
  -> Int64
  -> m (Pengguna, GrupSistem)
angkatAdmin reqr pid = do
  _ <- cekKewenanganMinimal reqr SuperAdmin
  runDb $ updatePengguna (toSqlKey pid) Nothing Nothing (Just Admin)
  lihatAdmin reqr (toSqlKey pid)
