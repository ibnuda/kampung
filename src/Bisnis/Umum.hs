{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Bisnis.Umum where

import           Protolude

import           Crypto.BCrypt
import           Data.ByteString.Char8 (pack)
import           Data.Text             (unpack)
import           Data.Time
import           Database.Esqueleto

import           Conf
import           Types
import           Util

import           Model
import           Model.Grouping

import           Qu.Meteran
import           Qu.Minum
import           Qu.Pengguna
import           Qu.Tarif

cekKewenanganMinimal
  :: (MonadError Gagal m, MonadReader Configuration m)
  => Pengguna
  -> GrupSistem
  -> m Pengguna
cekKewenanganMinimal pengguna@Pengguna {..} grouping = do
  grup <- asks configurationExistingGroups
  case find (\x -> penggunaGrupId == entityKey x) grup of
    Nothing -> throwError $ GagalDB "Grup Tidak Ada." "Cek Grup."
    Just (Entity _ (Grup y))
      | y <= grouping -> return pengguna
      | otherwise     -> throwError $ GagalTakBerwenang "Wewenang"

cekPenggunaAda
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text
  -> m (Entity Pengguna)
cekPenggunaAda telepon = do
  mpengguna <- runDb $ selectPelangganByNomorTelepon telepon
  case mpengguna of
    Nothing -> throwError $ GagalPenggunaNil telepon
    Just x  -> return x

cekPenggunaNil
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text
  -> m ()
cekPenggunaNil telepon = do
  mpengguna <- runDb $ selectPelangganByNomorTelepon telepon
  case mpengguna of
    Nothing -> return ()
    Just _  -> throwError $ GagalPenggunaAda telepon

cekMeteranAda
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text
  -> m (Entity Meteran)
cekMeteranAda nomormeteran = do
  mmeteran <- runDb $ getBy (UniqueNomor nomormeteran)
  case mmeteran of
    Nothing -> throwError $ GagalMeteranNil nomormeteran
    Just x  -> return x

cekMeteranNil
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text
  -> m ()
cekMeteranNil nomormeteran = do
  mmeteran <- runDb $ getBy (UniqueNomor nomormeteran)
  case mmeteran of
    Nothing -> return ()
    Just _  -> throwError $ GagalMeteranAda nomormeteran

cekPenggunaPunyaMeteran
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text
  -> m (Entity Meteran)
cekPenggunaPunyaMeteran notelp = do
  meteran <- runDb $ selectMeteranByNomorTelp notelp
  case meteran of
    []  -> throwError $ GagalPenggunaTunaMeteran
    x:_ -> return x

buatTokenLogin
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Text
  -> Text
  -> m (Pengguna, Text)
buatTokenLogin telepon password = do
  (Entity _ pengguna) <- cekPenggunaAda telepon
  unless
      ( validatePassword (pack . unpack $ penggunaPassword pengguna)
                         (pack . unpack $ password)
      )
    $ throwError GagalMasuk
  token <- generateToken pengguna
  return (pengguna, token)

tambahPengguna
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => GrupSistem -- ^ Target grup.
  -> Pengguna -- ^ Yang melakukan.
  -> Text -- ^ Nama
  -> Text -- ^ Telepon
  -> Text -- ^ Password
  -> Text -- ^ Alamat
  -> m (Entity Pengguna)
tambahPengguna grup admin nama telepon password alamat = do
  _ <- cekKewenanganMinimal admin Admin
  cekPenggunaNil telepon
  runDb $ insertPengguna nama telepon password grup alamat

lihatPenggunaByGrup
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => GrupSistem -- ^ Grup yang akan dilihat.
  -> Maybe (Key Pengguna) -- ^ Primary key yang dilihat. Opsional.
  -> Pengguna -- ^ Yang meminta.
  -> m [Entity Pengguna]
lihatPenggunaByGrup grup mid admin = do
  _ <- cekKewenanganMinimal admin Petugas
  runDb $ selectPenggunaByGrup mid grup

lihatPenggunaDanGrup
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Yang mencari.
  -> Key Pengguna -- ^ Yang dicari.
  -> m [(Entity Pengguna, Value GrupSistem)]
lihatPenggunaDanGrup admin mid = do
  _ <- cekKewenanganMinimal admin Petugas
  runDb $ selectPengguna mid

cekTarifTerbaru
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => m (Entity Tarif)
cekTarifTerbaru = do
  tarif <- runDb selectTarifTerbaru
  case tarif of
    []  -> throwError GagalTarifKosong
    x:_ -> return x

cekMinumAda
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text -- ^ Nomor Meteran.
  -> m (Entity Minum)
cekMinumAda nomormeteran = do
  sekarang <- liftIO getCurrentTime
  terisi   <- runDb $ selectMinumBulanIni nomormeteran $ utctDay sekarang
  case terisi of
    []  -> throwError $ GagalMinumNil nomormeteran
    x:_ -> return x

cekMinumNil
  :: (MonadError Gagal m, MonadReader Configuration m, MonadIO m)
  => Text -- ^ Nomor Meteran.
  -> m ()
cekMinumNil nomormeteran = do
  sekarang <- liftIO getCurrentTime
  terisi   <- runDb $ selectMinumBulanIni nomormeteran $ utctDay sekarang
  case terisi of
    [] -> return ()
    _  -> throwError $ GagalMinumAda nomormeteran

gantiPassword
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Pengguna -- ^ Requester.
  -> Text -- ^ Password lama.
  -> Text -- ^ Password baru.
  -> m ()
gantiPassword Pengguna {..} passwordlama passwordbaru = do
  _ <- cekPenggunaAda penggunaNomorTelp
  unless
      ( validatePassword (pack . unpack $ penggunaPassword)
                         (pack . unpack $ passwordlama)
      )
    $ throwError GagalPasswordBeda
  runDb $ updatePenggunaPassword penggunaNomorTelp passwordbaru

cekTahunBulanValid
  :: (MonadError Gagal m, MonadIO m)
  => Maybe Integer
  -> Maybe Int
  -> m (Integer, Int)
cekTahunBulanValid mtahun mbulan = do
  hari <- utctDay <$> liftIO getCurrentTime
  let (tahun, bulan, tanggal) = toGregorian hari
  case (mtahun, mbulan) of
    (Nothing, Nothing) -> return (tahun, bulan)
    (Nothing, _      ) -> throwError GagalTanggalTidakAda
    (_      , Nothing) -> throwError GagalTanggalTidakAda
    (Just x , Just y ) -> case fromGregorianValid x y tanggal of
      Nothing -> throwError $ GagalTanggalTidakValid x y
      Just d  -> do
        let (t, b, _) = toGregorian d
        if hari < d
          then throwError $ GagalTanggalBelumAda tahun bulan
          else return (t, b)

bulanLalu :: MonadError Gagal m => Integer -> Int -> m (Integer, Int)
bulanLalu tahun bulan = case fromGregorianValid tahun bulan 1 of
  Nothing -> throwError $ GagalTanggalTidakValid tahun bulan
  Just x  -> do
    let harisebelumnya            = addDays (-1) x
        (tahunlalu, bulanlalu, _) = toGregorian harisebelumnya
    return (tahunlalu, bulanlalu)

lihatRiwayatMinumByNomorTelepon
  :: (MonadIO m, MonadReader Configuration m, MonadError Gagal m)
  => Text -- ^ Nomor Telepon.
  -> m (Pengguna, Meteran, [Entity Minum]) -- ^ Hasil.
lihatRiwayatMinumByNomorTelepon nomortelepon = do
  (Entity _   p) <- cekPenggunaAda nomortelepon
  (Entity mid m) <- cekPenggunaPunyaMeteran nomortelepon
  minum          <- runDb $ selectMinumByMeteran mid
  return (p, m, minum)
